---
layout: homework
title: HW7 - Office Hours Queue
---

# Office Hours Queue

## Problem Description

You're an innovative TA for CS 1321. You've noticed your whiteboard queue system
often gets crowded and complicated on busy days. You decide that, since you
learned JavaFX when you took the course, you will implement an office hours
queue in JavaFX.

![Example application](screenshot4.png) ![Example pop-up](screenshot3.png)

_One possible design of the application_

## Solution Description

For this homework, your main task is to build a JavaFX application, called
`OfficeHourQueue` (this is the name of the class), which meets the following
functional requirements:
- Allows students to type their names into a text box and add themselves to the
queue by clicking a button
- Allow TAs to press a "dequeue" button to remove the student at the head of
the queue. This is a privileged action (see below for more details).
- Display the queue, with the most recently added students at the bottom of the
queue (lower on the screen).
- Display the current size of the queue, along with the maximum size the queue
has ever reached.
- For privileged actions, the program should display a pop-up asking the user
for a password. The correct password is "CS1321R0X". If the user enters an
invalid password, the action should have no effect. Otherwise the action should
proceed.

It is entirely possible to complete this assignment using a simple `LinkedList`
and updating a series of basic nodes within a `VBox`, however you may want to
consider using the javafx `ListView` class and some of the methods within
`FXCollections`. We will be grading based on the functionality of your GUI, so
how you decide to implement it is up to you.

**Note:** For `OfficeHourQueue`, you are allowed to import anything that
doesn't trivialize the assignment. For example, you may use
`java.util.LinkedList` or
`javafx.collections.FXCollections.observableArrayList()` as a collection for
storing students currently in the queue, or any any JavaFX classes you need.
You shouldn't import some `GUIQueueWithPasswordSupport` class that does the
entire assignment for you. If you aren't sure about a specific import, ask on
Piazza.

## Extra Credit

For extra credit, you have the option to implement your own queue instead of
using `LinkedList` or the FXCollections `ObservableArrayList` for keeping
track of the students in office hours. Your queue should be a linked
list contained in a **generic** class called `LinkedQueue<E>`, which must
extend
[ModifiableObservableListBase](https://docs.oracle.com/javase/8/javafx/api/javafx/collections/ModifiableObservableListBase.html)
and implement the following two interfaces: `Iterable`
and [SimpleQueue.java](SimpleQueue.java).
The [LinkedQueueNode.java](LinkedQueueNode.java) class has been provided to
help with your implementation.

You must use an instance of `LinkedQueue` for keeping track of students within
`OfficeHoursQueue` to receive extra credit. Thoroughly test your GUI to
ensure everything functions appropriately.

**Note:** For `LinkedQueue`, you are allowed to import `java.util.Iterator`,
`java.util.NoSuchElementException`, and
`javafx.collections.ModifiableObservableListBase`. **Do not import any lists,
sets, or collections from java.util.** If you want to use any other imports,
ask on Piazza.

## Rubric

This homework has 90 points of extra credit for a total of **190** points.

[100] **OfficeHourQueue**
- [10] Student name text box and buttons exist.
- [20] Dequeue button exists and removes students.
  - If you do not implement the privileged action pop-up, remove them anyways
  for credit.
- [30] Queue properly displays students, with the most recently added students
at the bottom.
- [20] Current queue size and historical maximum queue size displays.
- [10] Pop-up displays for privileged actions.
- [10] Actions do not take effect if invalid password is entered.

[90] **LinkedQueue**
- [10] `enqueue` adds an element to the queue
- [12] `doAdd` 
  - [10] Adds an element to the queue at a specific index
  - [2] Throws an `IndexOutOfBoundsException` if the index is invalid
- [12] `dequeue` 
  - [10] Removes the least recently added element when queue is non-empty
  - [2] Returns null when queue is empty
- [12] `doRemove` 
  - [10] Removes an element from the queue at a specific index
  - [2] Throws an `IndexOutOfBoundsException` if the index is invalid
- [12] `get` 
  - [10] Returns an element from the queue at a specific index
  - [2] Throws an `IndexOutOfBoundsException` if the index is invalid
- [2] `doSet` throws an `UnsupportedOperationException`
- [15] `LinkedQueue` is `Iterable<E>`
  - [5] Implements `Iterable<E>` and has compiling `iterator()` method
  - [5] `hasNext()` method properly implemented in Iterator
  - [5] `next()` method properly implemented in Iterator
- [5] `size()` method returns size of queue
- [5] `clear()` method empties queue and sets size to 0
- [5] `isEmpty()` method returns true when queue is empty, false otherwise

## Checkstyle

For each of your homework assignments we will run checkstyle and deduct one point for every checkstyle error.

There is **no checkstyle cap** for this assignment, meaning that the amount of points lost for checkstyle errors is not limited.

- If you encounter trouble running checkstyle, check Piazza for a solution and/or ask a TA as soon as you can!
- You can run checkstyle on your code by using the jar file found on the course website. To check the style of your code run  `java -jar checkstyle-6.2.2.jar -a *.java`.
- Javadoc errors are the same as checkstyle errors, as in each one is worth a single point and they are counted towards the checkstyle cap.
- **You will be responsible for running checkstyle on *ALL* of the code you submit, including provided code.**
- If you do choose to modify files in model or controller, you are responsible for ensuring that they remain checkstyle compliant.
- Depending on your editor, you might be able to change some settings to make it easier to write style-compliant code. See the [customization tips](http://cs1331.gatech.edu/customization-tips.html) page for more information.

## Submission

For this assignment, you will be submitting a zip file that contains any and all files needed for your submission to compile and run properly. Please be sure to compress all of your files and name the zip file following this naming convention: `GTID_hw7.zip`. The zip file you submit should look like this:

    gburdell3_hw7.zip


Submit all files necessary for your program to run. Do not submit `.class` files; only submit `.java` files. Check Piazza before submitting for important updates and clarifications.

_Please_ make sure you have included all the necessary files for your program to compile and run. If your submission does not compile because you forgot a file, or if it cannot run because an image is missing, we won't be able to award you any points no matter how small the error. Re-download and double check your submission in a different folder from your original project to make sure it works!

**NOTE: Please be sure to remove any class files before you zip up your project!!!**
