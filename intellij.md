---
layout: default
title: CS 1331 - IntelliJ
---

# Intellij

Almost all professional Java development teams use [IntelliJ IDEA](https://www.jetbrains.com/idea/), an IDE (integrated development environment) for Java and other JVM languages.  We also use IntelliJ in CS1332 and CS2340.  In the second half of CS1331 we will use IntelliJ so that you'll be prepared to hit the ground running in your next courses or internships.

## Get Started

1. Get IntelliJ
  - As a student or faculty member, you can get all of JetBrains's products free.  The easiest way is to visit [JetBrains's student license page](https://www.jetbrains.com/student/), click "APPLY NOW" and use your university email address.  You'll get an email within a few minutes with instructions on downloading the products in their "Product Pack for Students."

2. [Learn IntelliJ](https://www.jetbrains.com/help/idea/)

## Add a Few Customizations

- Editor
  - General
    - Soft Wraps 
      - Check "Use Soft Wraps in Editor"
    - Virtual Space 
      - Uncheck all.
    - Other
      - Strip trailing spaces on exit - All
      - Uncheck "Always keep trailing spaces on caret line"
      - Check "Ensure line feed at end of file on save"
  - Smart Keys
    - Uncheck "Insert paired brackets (), [], <>" 
    - Uncheck "Insert pair quote"
    - Check "Surround selection on typing quote or brace"

- Keymap (Note: these are macOS-specific.)
  - Main Menu
    - File
      - Open...: **CTRL-CMD-O**
  - Plugins
    - Terminal
      - Open in Terminal: **CTRL-F12**

## Keyboard Shortcuts

The more you keep your fingers on the keyboard, the faster you'll be.  Here are some shortcuts that I commit to memory.

> Note: for many commands, especially "switching" commands, adding a SHIFT reverses the direction.

**macOS**

- File|Open: **CTRL-CMD-O**
  > You can open any file or directory, so you can actually use IntelliJ as a general-purpose text editor.  To open an IDEA project, open the project root directory (e.g., directory with a `.idea` subdirectory).
- Switch between open projects: **CMD-`** (Window|Next Project Window)
- Goto/Toggle Project Pane: **CMD-1**
- Goto Editor: **ESC**
- Open in Terminal: **CTRL-F12**
- Goto/Toggle Terminal: **OPT-F12**
- Switch between open files or tool windows: **CTRL-TAB**

More to come ...

## Playing Nicely with Emacs

Emacs's keybindings are far different from modern applications, and many of these keybindings cause problems (e.g., M-w in Emacs copies the selection, but in macOS it closes the current window).  I tried Intellij's Emacs keymap and didn't like it.  It may be easier to simply modify Emacs's keybindings to match Intellij's.  Here are some Emacs config files to give you a start:

- For IntelliJ's Mac OS X 10.5+ keymap: [intellij.el](intellij.el)

More to come ...

## Miscellaneous Tips

- Add [JetBrains's .gitignore items](https://raw.githubusercontent.com/github/gitignore/master/Global/JetBrains.gitignore) to your [.gitignore](https://git-scm.com/docs/gitignore) file.
